from frameworks_from_scratch.hw_25_inheritance_abstraction_hiding.mammals import Mammals


class FlyingMammals(Mammals):
    def __init__(self, legs: int, tail: int, horns: bool, milk_liker: bool, wings: bool):
        super().__init__(legs, tail, horns, milk_liker)
        self.wings = wings

    def my_class(self):
        print(f'I am mammal and i like milk and flying')

if __name__ == '__main__':
    sparow = FlyingMammals(legs=4, tail=1, horns=True, milk_liker=False, wings=True)
    sparow.my_class()