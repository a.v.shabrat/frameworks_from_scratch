from frameworks_from_scratch.hw_25_inheritance_abstraction_hiding.mammals import Mammals


class WaterMammals(Mammals):
    def __init__(self, legs: int, tail: int, horns: bool, milk_liker: bool, nipper: bool):
        super().__init__(legs, tail, horns, milk_liker)
        self.nipper = nipper

    def my_class(self):
        print(f'I am rak and i like plying dota')


if __name__ == '__main__':
    rak = WaterMammals(legs=4, tail=1, horns=True, milk_liker=False, nipper=True)
    rak.my_class()