from abc import ABC, abstractmethod


class Tank(ABC):
    def __init__(self):
        self.crew = 4
        self.caliber = 120
        self.weight = None

    def my_crew(self):
        print(f'My crew is {self.crew} troopers')
        return self

    def my_caliber(self):
        print(f'I gun with {self.caliber}')
        return self

    @abstractmethod
    def my_weight(self):
        print(f'My weight is {self.weight} t.')


class Leopard(Tank):
    def __init__(self):
        super().__init__()
        self.weight = 75

    def my_weight(self):
        print(f'My weight is {self.weight} t.')


class Challenger(Tank):
    def __init__(self):
        super().__init__()
        self.weight = 60

    def my_weight(self):
        print(f'My weight is {self.weight} t.')

if __name__ == '__main__':
    challenger = Challenger()
    challenger.my_crew().my_caliber().my_weight()
    leopard = Leopard()
    leopard.my_crew().my_caliber().my_weight()

