class Animals:
    def __init__(self, legs: int, tail: int, horns: bool):
        self.kingdom = 'Animals'
        self.legs = legs
        self.tail = tail
        self.horns = horns

    def i_am(self):
        print(f'I belong to the animal kingdom')
