class Pig:
    def __init__(self):
        self.intellect = 10
        self.weight = 200


class Dog:
    def __init__(self):
        self.claw = True


class Montser(Pig, Dog):
    def __init__(self):
        Pig.__init__(self)
        Dog.__init__(self)

if __name__ == '__main__':
     monster = Montser()
     print(monster.claw)
     print(monster.weight)
