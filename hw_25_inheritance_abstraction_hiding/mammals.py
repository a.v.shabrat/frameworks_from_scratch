from frameworks_from_scratch.hw_25_inheritance_abstraction_hiding.animals import Animals


class Mammals(Animals):
    def __init__(self, legs: int, tail: int, horns: bool, milk_liker: bool):
        super().__init__(legs, tail, horns)
        self.milk_liker = milk_liker

    def my_class(self):
        print(f'I am mammal and i like milk')


# if __name__ == '__main__':
#     dog = Mammals(legs=4, tail=1, horns=True, milk_liker=False)
#     dog.my_class()