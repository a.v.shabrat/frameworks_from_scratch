from frameworks_from_scratch.mongo_db.BaseMongo import BaseMongo


class Collection(BaseMongo):
    def __init__(self):
        super().__init__()

    def update_one(self, key, value, new_key, new_value):
        old_value = {f"{key}": f"{value}"}
        new_value = {"$et": {f"{new_key}": f"{new_value}"}}
        self._my_col.update_one(old_value, new_value)