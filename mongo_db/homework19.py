from frameworks_from_scratch.mongo_db.BaseMongo import BaseMongo
from frameworks_from_scratch.mongo_db.collection_class import Collection

# BaseMongo().insert_one('iPhone 8-', 'Anatolik', 350, True)
# BaseMongo().insert_many([{"product": "iPone 7+", "name": "Artur", "budget": 400,
#                           "status": True}, {"product": "iPone 8+", "name": "Ira", "budget": 450,
#                                             "status": True}])
# BaseMongo().find_one('name', 'Anatolik')
# BaseMongo().find_all()
# BaseMongo().delete_one('name', 'Anatolik')
# BaseMongo().delete_all()
#Collection.update_one("name", "Ira", "age", 30)
