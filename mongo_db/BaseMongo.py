import pymongo

from frameworks_from_scratch.configurations.api_config import CLIENT_CONNECTION_URL, MY_DB, MY_COL


class BaseMongo:
    def __init__(self):
        self._client = pymongo.MongoClient(CLIENT_CONNECTION_URL)
        self._my_db = self._client[MY_DB]
        self._my_col = self._my_db[MY_COL]

    def insert_one(self, product: str, name: str, budget: int, status: bool):
        self._my_col.insert_one({"product": f'{product}', "name": f"{name}", "budget": f"{budget}",
                                 "status": f"{status}"})

    def insert_many(self, *dicts):
        self._my_col.insert_many(*dicts)

    def find_one(self, key, value):
        result = self._my_col.find_one({f'{key}': f'{value}'})
        return result

    def find_all(self):
        cursor = self._my_col.find()
        for homework19v2 in cursor:
            print(homework19v2)
        return cursor

    def delete_one(self, key, value):
        self._my_col.delete_one({f'{key}': f'{value}'})

    def delete_all(self):
        self._my_col.delete_many({})
