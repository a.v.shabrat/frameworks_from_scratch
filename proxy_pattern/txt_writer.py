from frameworks_from_scratch.proxy_pattern.abstract_writer import Writer


class TxtWriter(Writer):

    def __init__(self, file_path):
        self.file_path = file_path

    def write_file(self, new_data):
        with open(self.file_path, 'a') as file:
            text = file.write(new_data)
        return text

