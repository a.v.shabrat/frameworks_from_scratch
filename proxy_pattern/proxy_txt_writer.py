from frameworks_from_scratch.proxy_pattern.abstract_writer import Writer
from frameworks_from_scratch.proxy_pattern.txt_writer import TxtWriter


class TxtProxyWriter(Writer):
    def __init__(self, txt_writer: TxtWriter, ):
        self.__result = ''
        self.__is_actual = False
        self.__writer = txt_writer

    def write_file(self, new_data):
        if self.__is_actual:
            return self.__result
        else:
            self.__result = self.__writer.write_file(new_data)
            self.__is_actual = True
            return self.__result


if __name__ == '__main__':
    txt_writer = TxtWriter('txt_for_proxy_writer.txt')
    proxy_writer = TxtProxyWriter(txt_writer)
    print(proxy_writer.write_file('hello boy, how are u'))

