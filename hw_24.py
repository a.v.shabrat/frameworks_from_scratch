import os
import sys
from string import ascii_lowercase as letters

# 1 You have the file text.txt(attached). Please count how many times each letter appears in this file.
from turtle import fd

with open('text.txt', 'r') as f:
    file = f.read()
    result2 = dict((l, file.count(l)) for l in letters)
    print(result2)


# 2 Create your custom print function. It should run on any platform (Linux, Mac, Windows).
# You can not use build-in print (os module can help you with this task )

def my_print(some_str):
    return sys.stdout.write(some_str)

my_print(file)
