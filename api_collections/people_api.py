from frameworks_from_scratch.data_classes.person import Person
from frameworks_from_scratch.utilities.api.BaseAPI import BaseAPI
from frameworks_from_scratch.utilities.auto_step_decorator import auto_step


@auto_step
class PersonAPI(BaseAPI):
    def __init__(self):
        super().__init__()
        self.__url = '/public/v2/users'

    def get_person_by_id(self, person_id, headers=None):
        return self.get(f'{self.__url}/{person_id}', headers=headers)

    def create_person(self, body=None):
        person_data = Person()
        if body is not None:
            person_data.update_dict(**body)
        response = self.post(self.__url, body=person_data.get_json())
        return response

    def get_all_persons(self, headers=None):
        return self.get(f'{self.__url}', headers=headers)

    def delete_person_by_id(self, person_id, headers=None):
        return self.delete(f'{self.__url}/{person_id}', headers=headers)
