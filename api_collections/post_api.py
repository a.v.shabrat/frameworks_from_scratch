from frameworks_from_scratch.data_classes.post import Post
from frameworks_from_scratch.utilities.api.BaseAPI import BaseAPI
from frameworks_from_scratch.utilities.auto_step_decorator import auto_step


@auto_step
class PostAPI(BaseAPI):
    def __init__(self):
        super().__init__()
        self.__url_person_posts = '/public/v2/users/6368/posts'
        self.__url_posts = '/public/v2/posts'

    def create_post(self, body=None):
        post_data = Post()
        if body is not None:
            post_data.update_dict(**body)
        return self.post(self.__url_person_posts, body=post_data.get_json())

    def get_person_posts(self, headers=None):
        return self.get(f'{self.__url_person_posts}', headers=headers)

    def get_post_by_id(self, post_id, headers=None):
        return self.get(f'{self.__url_posts}/{post_id}', headers=headers)

    def delete_post_by_post_id(self, post_id, headers=None):
        return self.delete(f'{self.__url_posts}/{post_id}', headers=headers)
