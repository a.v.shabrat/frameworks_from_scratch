import pytest


@pytest.mark.regression
def test_search_function(open_main_page):
    main_page = open_main_page.search_function('клан сопрано')
    assert main_page.is_result_info_visible() is True


@pytest.mark.regression
def test_next_page_btn(open_main_page):
    main_page = open_main_page.next_page_btn()
    assert main_page.is_clickable_next_page_btn() is True


@pytest.mark.regression
def test_previous_page_btn(open_main_page):
    main_page = open_main_page.next_page_btn().next_page_btn().previous_page_btn()
    assert main_page.is_clickable_previous_page_btn() is True


@pytest.mark.regression
@pytest.mark.smoke
def test_next_swiper_function(open_main_page):
    main_page = open_main_page.next_swiper_function()
    assert main_page.is_clickable_next_swiper_function() is True


@pytest.mark.regression
@pytest.mark.smoke
def test_prev_swiper_function(open_main_page):
    main_page = open_main_page.prev_swiper_function()
    assert main_page.is_clickable_prev_swiper_function() is True
