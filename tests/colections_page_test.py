import pytest


@pytest.mark.regression
@pytest.mark.smoke
def test_enter_to_collection_page(open_main_page):
    colections_page = open_main_page.go_to_colections_page()
    assert colections_page.is_visible_colections_info_banner() is True


@pytest.mark.regression
def test_telegram_btn(open_main_page):
    colections_page = open_main_page.go_to_colections_page()
    assert colections_page.is_visible_and_clickable_telegram_btn() is True


@pytest.mark.regression
def test_whatsapp_btn(open_main_page):
    colections_page = open_main_page.go_to_colections_page()
    assert colections_page.is_visible_and_clickable_whatsapp_btn() is True


@pytest.mark.regression
def test_next_page_btn(open_main_page):
    colections_page = open_main_page.go_to_colections_page()
    assert colections_page.is_visible_and_clickable_next_page() is True


@pytest.mark.regression
def test_prev_next_page_btn(open_main_page):
    colections_page = open_main_page.go_to_colections_page()
    assert colections_page.is_visible_and_clickable_prev_page() is True
