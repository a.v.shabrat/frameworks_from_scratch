import pytest


@pytest.mark.regression
@pytest.mark.smoke
def test_info_banner(open_main_page):
    anonce_page = open_main_page.go_to_anonce_page()
    assert anonce_page.is_info_banner_visible() is True


@pytest.mark.regression
def test_reset_btn(open_main_page):
    anonce_page = open_main_page.go_to_anonce_page()
    assert anonce_page.is_clickable_rest_btn() is True


@pytest.mark.regression
def test_next_page_btn(open_main_page):
    anonce_page = open_main_page.go_to_anonce_page()
    anonce_page.next_page_btn()
    assert anonce_page.is_clickable_next_page_btn() is True


@pytest.mark.regression
def test_previous_page_btn(open_main_page):
    anonce_page = open_main_page.go_to_anonce_page()
    anonce_page.next_page_btn().next_page_btn().previous_page_btn()
    assert anonce_page.is_clickable_previous_page_btn() is True


@pytest.mark.regression
def test_twitter_btn_is_visible_and_clickable(open_main_page):
    anonce_page = open_main_page.go_to_anonce_page()
    assert anonce_page.is_visible_and_clickable_twitter_btn() is True
