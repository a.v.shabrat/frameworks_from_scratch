import pytest


@pytest.mark.regression
@pytest.mark.smoke
def test_emotion_info_banner(open_main_page):
    favorite_page = open_main_page.go_to_favorite_page()
    assert favorite_page.is_emotion_banner_visible() is True


@pytest.mark.regression
def test_emotion_dropdown(open_main_page):
    favorite_page = open_main_page.go_to_favorite_page()
    assert favorite_page.is_visible_ab_clickable_emotion_select_dropdown() is True


@pytest.mark.regression
def test_lovely_emoji_is_visible_anc_clickable(open_main_page):
    favorite_page = open_main_page.go_to_favorite_page()
    assert favorite_page.is_visible_and_clickable_lovely_emoji() is True


@pytest.mark.regression
def test_crazy_emoji_is_visible_anc_clickable(open_main_page):
    favorite_page = open_main_page.go_to_favorite_page()
    assert favorite_page.is_visible_and_clickable_crazy_emoji() is True


@pytest.mark.regression
def test_wow_emoji_is_visible_anc_clickable(open_main_page):
    favorite_page = open_main_page.go_to_favorite_page()
    assert favorite_page.is_visible_and_clickable_wow_emoji() is True
