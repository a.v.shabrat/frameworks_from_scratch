import allure
import pytest


# @pytest.mark.regression
def test_login_without_password(open_main_page):
    login_page = open_main_page.go_to_login_page().login_without_password('Arturich')
    assert login_page.is_login_button_visible() is True


# @pytest.mark.regression
def test_login_without_email(open_main_page):
    login_page = open_main_page.go_to_login_page().login_without_email('Qwerty123.')
    assert login_page.is_login_button_visible() is True


# @pytest.mark.regression
def test_login_wit_incorrect_email(open_main_page):
    login_page = open_main_page.go_to_login_page().login_with_incorrect_email('hw13@#!', 'Qwerty123.')
    assert login_page.is_login_button_visible() is True


# @pytest.mark.regression
def test_login_wit_incorrect_password(open_main_page):
    login_page = open_main_page.go_to_login_page().login_with_incorrect_password('hw14', 'Qwerty123')
    assert login_page.is_login_button_visible() is True


@pytest.mark.regression
@pytest.mark.smoke
@allure.feature('Artur Shabrat')
@allure.description('Login test')
@allure.severity('critical')
def test_login(open_main_page):
    with allure.step('Login func. verify'):
        login_page = open_main_page.go_to_login_page().login('hw14', 'Qwerty123.')
        assert login_page.is_personal_account_visible() is False
