from http import HTTPStatus

from frameworks_from_scratch.api_collections.post_api import PostAPI


def test_create_post():
    response = PostAPI().create_post()
    assert response.status_code == HTTPStatus.CREATED


def test_get_persons_post():
    response = PostAPI().get_person_posts()
    assert response.status_code == HTTPStatus.OK


def test_get_post_by_post_id():
    response = PostAPI().get_post_by_id(2612)
    assert response.status_code == HTTPStatus.OK


def test_delete_post_by_post_id():
    response = PostAPI().delete_post_by_post_id(2612)
    assert response.status_code == HTTPStatus.NO_CONTENT
