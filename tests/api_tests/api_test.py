import json

from http import HTTPStatus
from frameworks_from_scratch.api_collections.people_api import PersonAPI
from frameworks_from_scratch.data_classes.person import Person


def test_post_user():
    response = PersonAPI().create_person()
    assert response.status_code == HTTPStatus.CREATED


def test_get_person_by_id():
    response = PersonAPI().get_person_by_id(6368)
    assert response.status_code == HTTPStatus.OK


def test_response_body(create_person):
    expected_person = create_person
    response = PersonAPI().get_person_by_id(6368)
    from_json = json.loads(response.text)
    actual_person = Person.create_from_json(**from_json)
    assert actual_person == expected_person


def test_get_all_persons():
    response = PersonAPI().get_all_persons()
    assert response.status_code == HTTPStatus.OK


def test_delete_person_by_id():
    response = PersonAPI().delete_person_by_id()
    assert response.status_code == HTTPStatus.NO_CONTENT
