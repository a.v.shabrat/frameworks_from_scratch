from selenium.webdriver.common.by import By

from frameworks_from_scratch.page_objects.anonce_page import AnoncePage
from frameworks_from_scratch.page_objects.colections_page import ColectionsPage
from frameworks_from_scratch.page_objects.favorite_page import FavoritePage
from frameworks_from_scratch.page_objects.login_page import LoginPage
from frameworks_from_scratch.utilities.auto_step_decorator import auto_step
from frameworks_from_scratch.utilities.web_ui.base_page import BasePage


@auto_step
class MainPage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)

    __personal_account_btn = (By.XPATH, '//i[@class="fa fa-cog"]')
    __search_btn = (By.XPATH, '//i[@id="show-search"]')
    __search_field_input = (By.XPATH, '//input[@id="ajax_search"]')
    __submit_search_btn = (By.XPATH, '//*[@id="quicksearch"]/div/button')
    __result_info = (By.XPATH, '//div[@class="berrors"]')
    __pagination_next_btn = (By.XPATH, '//span[@class="pnext"]')
    __pagination_previous_btn = (By.XPATH, '//span[@class="pprev"]')
    __prev_swiper_btn = (By.XPATH, '//div[@class="swiper-button-prev swiper-prev-0"]')
    __next_swiper_btn = (By.XPATH, '//div[@class="swiper-button-next swiper-next-0"]')
    __anonce_page_btn = (By.XPATH, '//a[@href="/anonsi/"]')
    __logo_banner = (By.XPATH, '//a[@class="logo-box"]')
    __login_btn = (By.XPATH, '//i[@class="fa fa-user"]')
    __emotion_rate_btn = (By.XPATH, '//a[@href="/emoticon"]')
    __colections_page_btn = (By.XPATH, '//a[@href="/colections/"]')

    def is_personal_account_visible(self):
        return self._is_visible(self.__personal_account_btn)

    def search_function(self, value):
        self._click(self.__search_btn)
        self._send_keys(self.__search_field_input, value)
        self._click(self.__submit_search_btn)
        return self

    def is_result_info_visible(self) -> bool:
        return self._is_visible(self.__result_info)

    def next_page_btn(self):
        self._click(self.__pagination_next_btn)
        return self

    def previous_page_btn(self):
        self._click(self.__pagination_previous_btn)
        return self

    def is_clickable_next_page_btn(self) -> bool:
        return self._is_clickable(self.__pagination_next_btn)

    def is_clickable_previous_page_btn(self) -> bool:
        return self._is_clickable(self.__pagination_previous_btn)

    def next_swiper_function(self):
        self._click(self.__next_swiper_btn)
        return self

    def prev_swiper_function(self):
        self._click(self.__prev_swiper_btn)
        return self

    def is_clickable_next_swiper_function(self) -> bool:
        return self._is_clickable(self.__next_swiper_btn)

    def is_clickable_prev_swiper_function(self) -> bool:
        return self._is_clickable(self.__prev_swiper_btn)

    def go_to_anonce_page(self):
        self._click(self.__anonce_page_btn)
        return AnoncePage(self._driver)

    def go_to_login_page(self):
        self._click(self.__login_btn)
        return LoginPage(self._driver)

    def go_to_favorite_page(self):
        self._click(self.__emotion_rate_btn)
        return FavoritePage(self._driver)

    def go_to_colections_page(self):
        self._click(self.__colections_page_btn)
        return ColectionsPage(self._driver)

    def click_to_logo_banner(self):
        self._click(self.__logo_banner)
