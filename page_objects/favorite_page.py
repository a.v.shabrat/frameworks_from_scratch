from selenium.webdriver.common.by import By

from frameworks_from_scratch.utilities.auto_step_decorator import auto_step
from frameworks_from_scratch.utilities.web_ui.base_page import BasePage


@auto_step
class FavoritePage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)

    __emotion_rate_btn = (By.XPATH, '//a[@href="/emoticon"]')
    __emotion_info_banner = (By.XPATH, '//div[@class="emoji-category-header"]')
    __emotion_select_dropdown = (By.XPATH, '//div[@class="emoji-select-adv"]')
    __lovely_emotion_btn = (By.XPATH, '//a[@href="/emoticon/love/"]')
    __crazy_emotion_btn = (By.XPATH, '//a[@href="/emoticon/funny/"]')
    __wow_emotion_btn = (By.XPATH, '//a[@href="/emoticon/wow/"]')
    __piquant_emotion_btn = (By.XPATH, '//a[@href="/emoticon/piquant/"]')
    __shock_emotion_btn = (By.XPATH, '//a[@href="/emoticon/shock/"]')
    __dislike_emotion_btn = (By.XPATH, '//a[@href="/emoticon/dislike/"]')

    def click_to_emotion_rate_btn(self):
        self._click(self.__emotion_rate_btn)
        return self

    def is_emotion_banner_visible(self) -> bool:
        return self._is_visible(self.__emotion_info_banner)

    def is_visible_ab_clickable_emotion_select_dropdown(self) -> bool:
        self._click(self.__lovely_emotion_btn)
        return self._is_visible_and_clickable(self.__emotion_select_dropdown)

    def is_visible_and_clickable_lovely_emoji(self) -> bool:
        return self._is_visible_and_clickable(self.__lovely_emotion_btn)

    def is_visible_and_clickable_crazy_emoji(self) -> bool:
        return self._is_visible_and_clickable(self.__crazy_emotion_btn)

    def is_visible_and_clickable_wow_emoji(self) -> bool:
        return self._is_visible_and_clickable(self.__wow_emotion_btn)
