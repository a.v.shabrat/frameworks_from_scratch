from selenium.webdriver.common.by import By

from frameworks_from_scratch.utilities.auto_step_decorator import auto_step
from frameworks_from_scratch.utilities.web_ui.base_page import BasePage


@auto_step
class ColectionsPage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)

    __colections_page_btn = (By.XPATH, '//a[@href="/colections/"]')
    __colections_info_banner = (By.XPATH, '//h1[@class="title_desc_site coloredgray"]')
    __telegram_btn = (By.XPATH, '//a[@class="at-icon-wrapper at-share-btn at-svc-telegram"]')
    __whatsapp_btn = (By.XPATH, '//a[@class="at-icon-wrapper at-share-btn at-svc-whatsapp"]')
    __next_page_btn = (By.XPATH, '//span[@class="pnext"]')
    __prev_page_btn = (By.XPATH, '//span[@class="pprev"]')

    def click_to_colections_btn(self):
        self._click(self.__colections_page_btn)
        return self

    def is_visible_colections_info_banner(self) -> bool:
        return self._is_visible(self.__colections_info_banner)

    def is_visible_and_clickable_telegram_btn(self) -> bool:
        return self._is_visible_and_clickable(self.__telegram_btn)

    def is_visible_and_clickable_whatsapp_btn(self) -> bool:
        return self._is_visible_and_clickable(self.__whatsapp_btn)

    def is_visible_and_clickable_next_page(self) -> bool:
        return self._is_visible_and_clickable(self.__next_page_btn)

    def is_visible_and_clickable_prev_page(self) -> bool:
        return self._is_visible_and_clickable(self.__prev_page_btn)
