from selenium.webdriver.common.by import By

from frameworks_from_scratch.utilities.auto_step_decorator import auto_step
from frameworks_from_scratch.utilities.web_ui.base_page import BasePage


@auto_step
class AnoncePage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)

    __reset_btn = (By.XPATH, '//input[@data-dlefilter="reset"]')
    __anonce_page_btn = (By.XPATH, '//a[@href="/anonsi/"]')
    __info_banner = (By.XPATH, '//h1[@class="title_desc_site coloredgray"]')
    __pagination_next_btn = (By.XPATH, '//span[@class="pnext"]')
    __pagination_previous_btn = (By.XPATH, '//span[@class="pprev"]')
    __twitter_btn = (By.XPATH, '//a[@title="twitter"]')

    def click_to_anonce_page_btn(self):
        self._click(self.__anonce_page_btn)
        return self

    def is_clickable_rest_btn(self) -> bool:
        return self._is_clickable(self.__reset_btn)

    def is_info_banner_visible(self) -> bool:
        return self._is_visible(self.__info_banner)

    def next_page_btn(self):
        self._click(self.__pagination_next_btn)
        return self

    def previous_page_btn(self):
        self._click(self.__pagination_previous_btn)
        return self

    def is_clickable_next_page_btn(self) -> bool:
        return self._is_clickable(self.__pagination_next_btn)

    def is_clickable_previous_page_btn(self) -> bool:
        return self._is_clickable(self.__pagination_previous_btn)

    def is_visible_and_clickable_twitter_btn(self):
        return self._is_visible_and_clickable(self.__twitter_btn)
