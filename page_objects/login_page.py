from selenium.webdriver.common.by import By

from frameworks_from_scratch.utilities.auto_step_decorator import auto_step
from frameworks_from_scratch.utilities.web_ui.base_page import BasePage


@auto_step
class LoginPage(BasePage):
    def __init__(self, driver):
        self.__driver = driver
        super().__init__(driver)

    __login_btn = (By.XPATH, '//i[@class="fa fa-user"]')
    __email_input_field = (By.XPATH, '//input[@id="login_name"]')
    __password_input_field = (By.XPATH, '//input[@id="login_password"]')
    __enter_to_site_btn = (By.XPATH, '//button[@onclick="submit();"]')
    __personal_account_btn = (By.XPATH, '//i[@class="fa fa-cog"]')

    def login_button(self):
        self._click(self.__login_btn)

    def set_email(self, email):
        self._send_keys(self.__email_input_field, email)
        return self

    def set_password(self, password):
        self._send_keys(self.__password_input_field, password)
        return self

    def enter_to_site_button(self):
        self._click(self.__enter_to_site_btn)

    def login(self, email, password):
        self.set_email(email)
        self.set_password(password)
        self.enter_to_site_button()
        return self

    def is_login_button_visible(self) -> bool:
        return self._is_visible(self.__login_btn)

    def login_without_password(self, email):
        self.set_email(email)
        self.enter_to_site_button()
        return self

    def login_without_email(self, password):
        self.set_password(password)
        self.enter_to_site_button()
        return self

    def login_with_incorrect_password(self, email, password):
        self.set_email(email)
        self.set_password(password)
        self.enter_to_site_button()
        return self

    def login_with_incorrect_email(self, email, password):
        self._click(self.__login_btn)
        self.set_email(email)
        self.set_password(password)
        self.enter_to_site_button()
        return self

    def is_personal_account_visible(self):
        return self._is_visible(self.__personal_account_btn)
