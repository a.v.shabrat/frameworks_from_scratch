import json
from contextlib import suppress

import allure
import pytest
from selenium.webdriver.support.wait import WebDriverWait

from frameworks_from_scratch.page_objects.anonce_page import AnoncePage
from frameworks_from_scratch.page_objects.colections_page import ColectionsPage
from frameworks_from_scratch.page_objects.favorite_page import FavoritePage
from frameworks_from_scratch.page_objects.login_page import LoginPage
from frameworks_from_scratch.page_objects.main_page import MainPage
from frameworks_from_scratch.utilities.configuration import Configuration
from frameworks_from_scratch.utilities.configurations import Configuration
from frameworks_from_scratch.utilities.driver_factory import DriverFactory
from frameworks_from_scratch.utilities.read_configs import ReadConfig
from frameworks_from_scratch.data_classes.person import Person


@pytest.fixture(scope='session')
def env():
    with open(
            r"C:\Users\hp\PycharmProjects\frameworks_from_scratch\frameworks_from_scratch\configurations"
            r"\configurations.json") as file:
        env_dict = json.loads(file.read())
    return Configuration(**env_dict)


@pytest.hookimpl(hookwrapper=True, tryfirst=True)
def pytest_runtest_makereport(item, call):
    outcome = yield
    rep = outcome.get_result()
    setattr(item, "rep_" + rep.when, rep)

@pytest.fixture()
def create_driver(env, request):
    driver = DriverFactory.create_driver(driver_id=env.browser_id)
    driver.maximize_window()
    driver.get(env.base_url)
    yield driver
    if request.node.rep_call.failed:
        with suppress(Exception):
            allure.attach(driver.get_screenshot_as_png(), name=request.function.__name__,
                          attachment_type=allure.attachment_type.PNG)
    driver.quit()


# @pytest.fixture(scope='session')
# def create_driver():
# driver = DriverFactory.create_driver(ReadConfig.get_browser_id())
# driver.get(ReadConfig.get_base_url())
# yield driver
# driver.quit()


@pytest.fixture()
def open_main_page(create_driver):
    return MainPage(create_driver)


@pytest.fixture()
def base_api_url(env):
    base_api_url = env.base_api_url
    return base_api_url()


@pytest.fixture()
def x_api_key(env):
    x_api_key = env.x_api_key
    return x_api_key


@pytest.fixture()
def create_person():
    return Person()
