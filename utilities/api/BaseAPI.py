from frameworks_from_scratch.configurations.api_config import BASE_API_URL, API_KEY
from frameworks_from_scratch.conftest import base_api_url, x_api_key
import requests
import json


class BaseAPI:
    def __init__(self):
        self.__base_api_url = BASE_API_URL
        self.__user = 'admin'
        self.__headers = {'Content-Type': 'application/json', 'Authorization': 'Bearer {}'.format(API_KEY)}
        self.__request = requests

    def get(self, url, headers=None):
        if headers is None:
            headers = self.__headers
        response = self.__request.get(f'{self.__base_api_url}{url}', headers=headers)
        return response

    def post(self, url, body=None, headers=None):
        if headers is None:
            headers = self.__headers
        response = self.__request.post(f'{self.__base_api_url}{url}', headers=headers, data=body)
        return response

    def delete(self, url, headers=None):
        if headers is None:
            headers = self.__headers
        response = self.__request.delete(f'{self.__base_api_url}{url}', headers=headers)
        return response

