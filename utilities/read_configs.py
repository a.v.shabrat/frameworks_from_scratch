import configparser

abs_path = r'C:\Users\hp\PycharmProjects\frameworks_from_scratch\frameworks_from_scratch\configurations' \
           r'\configurations.ini '
config = configparser.RawConfigParser()
config.read(abs_path)


class ReadConfig:
    @staticmethod
    def get_base_url():
        return config.get('app_info', 'base_url')

    @staticmethod
    def get_user_name():
        return config.get('user_data', 'login')

    @staticmethod
    def get_user_password():
        return config.get('user_data', 'password')

    @staticmethod
    def get_browser_id():
        return config.get('browser_data', 'browser_id')
