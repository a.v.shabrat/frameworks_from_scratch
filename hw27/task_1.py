class Conductor:
    def __init__(self, hands: int, order_number: int, salary: int):
        self.hands = hands
        self.order_number = order_number
        self.salary = salary

    def __str__(self):
        return f"{self.__class__.__name__}: {{\t\n rooms: {self.hands}\t\n flat_number: {self.order_number}" \
               f"\t\n floor: {self.salary}\t\n}}"


print(Conductor(2, 228, 15000))
