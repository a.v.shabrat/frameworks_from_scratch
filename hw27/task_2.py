import random


class Train:
    def __init__(self):
        self.head = '<=[HEAD]'
        self.wagons = ''
        self.wagons_list = []
        for i in range(3):
            car = Wagon(i + 1).wagon
            setattr(self, f'wagon{i}', car)
            self.wagons_list.append(car)
        self.new_train()
        self.train = ''

    def new_train(self):
        self.wagons = ''.join(self.wagons_list)
        return self.head + self.wagons

    def __str__(self):
        return self.head + self.wagons

    def __add__(self, other):
        return self.head + self.wagons + other.wagon

    def __len__(self):
        print(self.wagons_list)
        return self.wagons_list.__len__()


class Wagon:
    def __init__(self, wagon_number):
        self.wagon = f'-[{wagon_number}]'
        self.count_of_passengers = random.randint(0, 50)

    def __len__(self):
        print(self.count_of_passengers)
        return self.count_of_passengers


wagon = Wagon(1)
result = Train() + wagon
print(result)

len(wagon)
len(Train())
print(Train())
