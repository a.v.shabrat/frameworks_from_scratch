from frameworks_from_scratch.hw26.hero_interface import IHeroe
from frameworks_from_scratch.hw26.skills_interface import ISkill


# Multiple inheritance
class Axe(IHeroe, ISkill):
    # hiding
    # encapsulation
    def __init__(self, damage_received):
        self.__move_speed = 310
        self.__health = 970
        self.__mana = 400
        self.__die = False
        self.__appear = True
        self.__damage_received = damage_received

    def appear(self):
        print('Axe is ready!')
        self.__appear = True
        self.__health = self.__health
        self.__mana = self.__mana
        self.__die = False

    # encapsulation
    def die(self, damage_received):
        print('There is no team in Axe!')
        self.__die = True
        self.__health = 0
        self.__appear = False
        self.__damage_received(damage_received)

    def use_agro(self):
        print('Let the carnage begin.')
        self.__appear = True
        self.__die = False
        self.__mana = -100

    def use_blink(self):
        print('The Axe-man comes!')
        self.__appear = True
        self.__die = False
        self.__mana = -50

    @property
    def show_mana(self):
        return print(self.__mana)
