from abc import ABC, abstractmethod


class ISkill(ABC):

    @abstractmethod
    def use_agro(self): ...

    @abstractmethod
    def use_blink(self): ...
