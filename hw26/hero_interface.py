from abc import ABC, abstractmethod


class IHeroe(ABC):
    # abstraction
    @abstractmethod
    def appear(self): ...

    @abstractmethod
    def die(self, damage_received): ...



