import json
from argparse import ArgumentParser
from dicttoxml import dicttoxml

parser = ArgumentParser(description='parser test')
parser.add_argument('--parser_type', help='init parser type', default='json')
argument = parser.parse_args()


class Human:
    def __init__(self, name, age, gender, birth_year):
        self.name = name
        self.age = age
        self.gender = gender
        self.birth_year = birth_year

    def convert_to_json(self):
        return self.__dict__

    def convert_to_xml(self):
        return self.__dict__


if argument.parser_type == 'json':
    with open('json_output.json', 'w+') as fp:
        human_to_json = Human('Artur', 26, 'male', 1996).convert_to_json()
        to_json = json.dumps(human_to_json)
        fp.write(to_json)

elif argument.parser_type == 'xml':
    with open('xml_output.xml', 'wb') as fp:
        human_to_xml = Human('Artur', 26, 'male', 1996).convert_to_xml()
        xml = dicttoxml(human_to_xml, custom_root='test', attr_type=False)
        fp.write(xml)


