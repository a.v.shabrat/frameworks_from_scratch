import psycopg2

from frameworks_from_scratch.db_repositories.base_repository import BaseRepository


class ProductRepository(BaseRepository):
    def __init__(self):
        super().__init__()
        self.table_name = 'products'


    def get_product_by_id(self, product_id):
        self._cursor.execute(f"select * from {self.table_name} where {self.table_name}.id = {product_id};")
        return self._cursor.fetchone()

    def get_inner_join(self):
        self._cursor.execute(f"select p.name, p.price, o.quantiti, p.price*o.quantiti as total from {self.table_name} as p "
                             f"inner join orders as o on p.id = o.product_id;")
        results = self._cursor.fetchall()
        for i in results:
            print(i)

    def insert_one(self, name, price):
        self._cursor.execute(f"insert into {self.table_name} (name, price) values ('{name}', '{price}');")
        self._connection.commit()

    def delete_by_id(self, product_id):
        self._cursor.execute(f"delete from {self.table_name} where {self.table_name}.id = {product_id}")
        self._connection.commit()
