import psycopg2

from frameworks_from_scratch.db_repositories.base_repository import BaseRepository


class OrdersRepository(BaseRepository):
    def __init__(self):
        super().__init__()
        self.table_name = 'orders'

    def get_order_by_id(self, product_id):
        self._cursor.execute(f"select * from {self.table_name} where {self.table_name}.id = {product_id};")
        return self._cursor.fetchone()

    def insert_one(self, name, price):
        self._cursor.execute(f"insert into {self.table_name} (name, price) values ('{name}', '{price}');")
        self._connection.commit()

    def delete_by_id(self, product_id):
        self._cursor.execute(f"delete from {self.table_name} where {self.table_name}.id = {product_id}")
        self._connection.commit()
