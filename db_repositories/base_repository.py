import psycopg2


class BaseRepository:
    def __init__(self):
        self._connection = psycopg2.connect(
                                            user='postgres',
                                            password='1111', host='localhost',
                                            port='5432',
                                            database='hw17'
                                            )
        self._cursor = self._connection.cursor()
        self.table_name = ''

    def get_all(self):
        self._cursor.execute(f"select * from {self.table_name};")
        return self._cursor.fetchall()

    # def inner_join(self):
    #     self._cursor.execute(f"select p.name, p.price, o.quantiti, p.price*o.quantiti as total from products as p "
    #                          f"inner join orders as o on p.id = o.product_id;")
    #     results = self._cursor.fetchall()
    #     for i in results:
    #         print(i)

    def __del__(self):
        if self._connection:
            if self._cursor:
                self._cursor.close()
            self._connection.close()